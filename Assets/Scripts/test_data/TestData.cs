namespace Excel_To_SQLite_WPF.Data
{
    public class TestData : ICustomData
    {
        public int id { get; set; }
        public string name { get; set; }
        public int count { get; set; }

        public TestData() { }

        public TestData(int id, string name, int count)
        {
            this.id = id;
            this.name = name;
            this.count = count;
        }
    }
}